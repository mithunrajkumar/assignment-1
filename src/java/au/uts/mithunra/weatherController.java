/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.uts.mithunra;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.http.*;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mithun
 */
@Named
@SessionScoped
//controller for the weatherDAO which performs all the functions related to weather
public class weatherController implements Serializable{

//gets the new faces context names session
FacesContext facesContext = FacesContext.getCurrentInstance();
HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);

RequestContext context = RequestContext.getCurrentInstance();
//creates the properties to be used by the bean
    private weatherDTO selectedWeather = new weatherDTO();
    private final weatherDTO newWeather;
    
    /**
     * used to access hte newWeather variable
     * @return the weather variable
     */
    public weatherDTO getNewWeather(){
    return newWeather;
    }
/**
 * used for getting selected data from the weather table
 * @return the selected weather from the table displaying the weather records
 */
    public weatherDTO getSelectedWeather() {
        return selectedWeather;
    }
/**
 * 
 * @param selectedWeather to pass the data to be set as the selected weather 
 */
    public void setSelectedWeather(weatherDTO selectedWeather) {
        this.selectedWeather = selectedWeather;
    }
    
    /**
     * constructs the properties of the bean
     */
    public weatherController() {
        this.newWeather = new weatherDTO();
        this.selectedWeather=new weatherDTO();
    }
         /**
          * this method is used to insert the weather record into the database
          * @throws SQLException if there is a problem connecting to the database
          * @throws NamingException if there is a problem connecting with the pool JNDI (jdbc/weather)
          * @throws IOException if there is a problem accessing the properties used
          */
    public void insertWeather() throws SQLException, NamingException, IOException{
        System.out.println("something happened");
        String userrecord = (String)session.getAttribute("use");
        newWeather.setRecordedBy(userrecord);
        weatherDAO.insert(newWeather);
    }
    /**
     * this method is used for updating the weather records
     * @throws SQLException if there is a problem connecting to the database
     * @throws NamingException if there is a problem connecting with the pool JNDI (jdbc/weather)
     */
    public void updateWeather() throws SQLException, NamingException{
        //calls th eupdate method in the weatherDAO
    weatherDAO.update(selectedWeather);
    }
    /**
     * this method is used for redirectin to the properfunction after checking the access permission of the record for the user
     * @param method this is used for passing the string specifying the action to be performed
     * @return the action to be performed after the weather record to be updated
     * @throws SQLException if there is a problem connecting to the database
     * @throws NamingException if there is a problem connecting with the pool JNDI (jdbc/weather)
     * @throws IOException if there is a problem accessing the properties used
     */
    public String tryUpdate(String method) throws SQLException, NamingException, IOException{
        String seshuser = (String)session.getAttribute("use");
        String recorduser = selectedWeather.getRecordedBy();
        String functionToPerform = "";
        //checks if user can manipulate the record
    if(seshuser.equals(recorduser)){
        System.out.println("done");
        if(method.equals("edit"))
        {
            //redirects to teh edit record page
            functionToPerform = "editRecord?faces-redirect=true";
        } 
        else if(method.equals("delete"))
        {
            //calls the delete method in th eDAO to delete the selected weather record from the database
            weatherDAO.delete(selectedWeather);
            //redirect to view all the records after deletion is complete
            ExternalContext mithun = FacesContext.getCurrentInstance().getExternalContext();
mithun.redirect(mithun.getRequestContextPath()+"/faces/viewrecords.xhtml");
        }
return functionToPerform;    
    }
    else{
        //informs user that he cannot access the record
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Access Denied", "Record is not yours");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
return "nothing";
    }
    }
    
    /**
     * this method is used to fetch all the weather records from the table
     * @return the list of weather records to be displayed in the weather table
     * @throws SQLException if there is a problem connecting to the database
     * @throws NamingException if there is a problem connecting with the pool JNDI (jdbc/weather)
     */
    public List<weatherDTO> getWeatherList() throws SQLException, NamingException{
        //creates the new arraylist to store the data 
        List<weatherDTO> weatherlist=new ArrayList<weatherDTO>();
        //calls the view method in the weatherDAO to view the results
        weatherlist = weatherDAO.view();
        return weatherlist;
    }
    
}
