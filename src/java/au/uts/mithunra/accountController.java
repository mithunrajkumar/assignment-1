/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.uts.mithunra;

import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import javax.enterprise.context.*;
import javax.faces.application.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.primefaces.context.RequestContext;


/**
 *
 * @author mithun
 */
/**
 *
 * @author Mithun
 */
@Named
//this class will be session scoped
@SessionScoped
/**
 * this class is responsible for controlling the account methods - login and signup
 */
public class accountController implements Serializable{
    /**
     the properties of the bean
     */
    private accountDTO userss;
    private accountDTO myInfo;
    private loginDTO logge;
/**
 * 
 * @return returns the signup data
 */
    public accountDTO getUserss(){
    return userss;
    }
/**
 * 
 * @return returns the users information
 */
    public accountDTO getMyInfo() {
        return myInfo;
    }
/**
 * 
 * @param myInfo used to set the user info
 */
    public void setMyInfo(accountDTO myInfo) {
        this.myInfo = myInfo;
    }
    
    /**
     * initialies the session variable with the name session
     */
FacesContext facesContext = FacesContext.getCurrentInstance();
HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
    /**
     * used to obtain the login information
     * @return returns the login information 
     */
    public loginDTO getLogge(){
    return logge;
    }
    /**
     * constructs the properties of the bean
     */
    public accountController() {
        this.logge = new loginDTO();
        this.userss = new accountDTO();
        this.myInfo=new accountDTO();
    }
    
    /**
     * 
     * @return the url to be redirected to insert record screen  
     */
    public String insertRedirect(){
        System.out.println("redirecting");
    return "insertRecord?faces-redirect=true";
    }
    /**
     * checks the session variable signin to popup a notification on the screen for success of signup
     */
    public void checkSignup(){
    if(("yes").equals(session.getAttribute("signin"))){
    
        FacesMessage message = new FacesMessage(FacesMessage.FACES_MESSAGES, "Signup Success");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    /*changes the session attribute once the signup is complete
    */
    session.setAttribute("signin","no");
        this.userss = new accountDTO();
    }
    /**
     * this method inserts the new user account into the database by passing userss data into the method insertInto in the accountDAO
     * @throws SQLException if there is a problem connecting with the database
     * @throws IOException if there is a problem accessing the sub methods used inside
     * @throws NamingException if there is a problem connecting to the JNDI pool (jdbc/weather)
     */
    public void insertAccount() throws SQLException, IOException, NamingException{
        accountDAO.insertInto(getUserss());
    }
    
    /**
     * 
     * @return
     * @throws NoSuchAlgorithmException
     * @throws SQLException if there is a problem connecting with the database
     * @throws IOException if there is a problem accessing the sub methods used inside
     * @throws NamingException if there is a problem connecting to the JNDI pool (jdbc/weather)
     */
    public String encryptAndInsert() throws NoSuchAlgorithmException, SQLException, IOException, NamingException {
        String passwordEncrypt=userss.getPassword();
    MessageDigest md = MessageDigest.getInstance("SHA-256");
    md.update(passwordEncrypt.getBytes());
    return bytesToHex(md.digest());
  }
  public String bytesToHex(byte[] bytes) throws SQLException, IOException, NamingException {
    StringBuffer result = new StringBuffer();
    for (byte byt : bytes) {
      result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
    }
    String encryptedPassword = result.toString();
    userss.setPassword(encryptedPassword);
    insertAccount();
    return result.toString();
    
  }
  /**
   * updates the user information by calling the method update in the accountDAO
   * @throws SQLException if there is a problem in connecting to the database
   * @throws NamingException is there is a problem in reachin the JNDI (jdbc/weather)
   */
   public void updateUserInfo() throws SQLException, NamingException{
        //calls the update method in the accountDAO
    accountDAO.update(myInfo);
    }
  /**
   * 
   * @throws SQLException if there is a problem in connecting to the database
   * @throws NamingException is there is a problem in reachin the JNDI (jdbc/weather)
   */
  public void getUserInfo() throws SQLException, NamingException{
  String seshionuser=(String)session.getAttribute("use");
  setMyInfo(accountDAO.getUserInfo(seshionuser));
  }
  
    /**
     * this method is used for logging in the user
     * @return the output of login
     * @throws SQLException if there is a problem connecting with the database
     */
    public String loginContainer() throws SQLException {
session.setAttribute("use", logge.getUsername());
String loginOutput = accountDAO.login(logge);
return loginOutput;
    }
    
    /**
     * this method is used for logging out the user
     * @return the url to be redirected after logging out
     * @throws ServletException if there is an problem connecting with the database 
     */
   public String logoutContainer() throws ServletException {
        this.logge = new loginDTO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        request.logout();
        System.out.println("logging out");
        return "login.xhtml?faces-redirect=true";
    }
    
}
