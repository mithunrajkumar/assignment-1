/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.uts.mithunra;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Mithun
 */
//used for accessing the weatherDTO transfer object
public class weatherDAO implements Serializable{

    /**
     * this method is used for inserting the user typed weather record into the database
     * @param DAOweather passes the object of the type weatherDTO to be inserted
     * @throws SQLException if there is a problem connecting to the database
     * @throws NamingException if there is a problem connecting to the JNDI pool (jdbc/weather)
     * @throws IOException if there is a problem accessing the IO parameters for the method
     */
        public static void insert(weatherDTO DAOweather) throws SQLException, NamingException, IOException {
        
        //connecting tho the pool
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
        Connection conn = ds.getConnection();
        String insertString = "INSERT INTO MITHUN.WEATHER (RECORDED_BY, RECORDED_DATE, TEMPERATURE, HUMIDITY, CONDITIONS, COMMENTS)" +
	"VALUES (?,?,?,?,?,?)";
        
        PreparedStatement insertWeather = conn.prepareStatement(insertString);
        //setting the parametes to the prepared statement
        insertWeather.setString(1, DAOweather.getRecordedBy());
        insertWeather.setString(2, DAOweather.getRecordedDate());
        insertWeather.setInt(3, DAOweather.getTemperature());
        insertWeather.setInt(4, DAOweather.getHumidity());
        insertWeather.setString(5, DAOweather.getCondition());
        insertWeather.setString(6, DAOweather.getComments());
        
        try{
            //execute the prepared statement
             insertWeather.executeUpdate();   
             //close the connection
             conn.close();
        //redirecting to the viewRecords page after successful insertion of the weather record
      ExternalContext mithun = FacesContext.getCurrentInstance().getExternalContext();
      mithun.redirect(mithun.getRequestContextPath()+"/faces/viewrecords.xhtml");
        }
        catch(SQLException e){
            //pops up the error message incase of invalid date
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Insert Failed", "Please check the Date provided");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }
        /**
         * this method is used to update the weather record
         * @param DAOweather the object of the type weatherDTO to be passed for updating the weather record
         * @throws SQLException if there is a problem connecting to the database
         * @throws NamingException if there is a problem connecting to the JNDI pool (jdbc/weather)
         */
        public static void update(weatherDTO DAOweather) throws SQLException, NamingException{
            //gets the id for the weather record to be updated
        int weatherId = (int)DAOweather.getId();
        //establishes the connection with the pool
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
        Connection conn = ds.getConnection();
        String updateString = "UPDATE WEATHER SET RECORDED_DATE=?, TEMPERATURE=?,HUMIDITY=?,CONDITIONS=?,COMMENTS=? WHERE ID=? and RECORDED_BY=?";
        //creates the prepared statement
            PreparedStatement updateWeather = conn.prepareStatement(updateString);
               updateWeather.setString(1,DAOweather.getRecordedDate());
               updateWeather.setInt(2,DAOweather.getTemperature());
               updateWeather.setInt(3,DAOweather.getHumidity());
               updateWeather.setString(4,DAOweather.getCondition());
               updateWeather.setString(5,DAOweather.getComments());
               updateWeather.setInt(6,weatherId);
               updateWeather.setString(7,DAOweather.getRecordedBy());
               //executes the prepared statement
        try{updateWeather.executeUpdate();  
        conn.close();
        //displays the message that the record has been inserted successfully
        FacesMessage message = new FacesMessage(FacesMessage.FACES_MESSAGES, "Record Updated");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
        catch (SQLException e){
            //displays the error message incase of any exceptions
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Update Failed", "Please try again later");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
        
    }
        /**
         * deletes the weather record from the databse
         * @param DAOweather the object of type weatherDTO to be deleted from the database
         * @throws SQLException if there is a problem connecting to the database
         * @throws NamingException if there is a problem connecting to the JNDI pool (jdbc/weather)
         */
         public static void delete(weatherDTO DAOweather) throws SQLException, NamingException{
             //get the id for the record to be deleted
        int weatherId = (int)DAOweather.getId();
        //initializes the datastore for establishing pool connection
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
        String deleteString = "DELETE FROM WEATHER WHERE ID=? and RECORDED_BY=?";
        Connection conn = ds.getConnection();
        //creates the prepared statement
        PreparedStatement deleteWeather = conn.prepareStatement(deleteString);
        deleteWeather.setInt(1,weatherId);
        deleteWeather.setString(2, DAOweather.getRecordedBy());
        //deletes the weather record
        deleteWeather.executeUpdate();  
        conn.close();
    }
         /**
          * this method is used to view the weather records in the database
          * @return the list of weather records to be shown in the weather table
         * @throws SQLException if there is a problem connecting to the database
         * @throws NamingException if there is a problem connecting to the JNDI pool (jdbc/weather)
          */
         public static List<weatherDTO> view() throws NamingException, SQLException{
         
                    
        String viewString = "select id,recorded_by,recorded_date,temperature,humidity,conditions,comments " +
                       "from weather";
        //creates teh connection with the pool
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
             Connection conn = ds.getConnection();
             PreparedStatement viewWeather = conn.prepareStatement(viewString);
             ResultSet rs = viewWeather.executeQuery();
             conn.close();
             //creates an arraylist of type weatherDTO for storing the data
             List<weatherDTO> weatherlist = new ArrayList<weatherDTO>();
		//saves all the data in teh arraylist
             while(rs.next()){
			weatherDTO weatherlis = new weatherDTO();

			weatherlis.setId(rs.getInt("id"));
			weatherlis.setRecordedBy(rs.getString("recorded_by"));
			weatherlis.setRecordedDate(rs.getString("recorded_date"));
			weatherlis.setTemperature(rs.getInt("temperature"));
			weatherlis.setHumidity(rs.getInt("humidity"));
			weatherlis.setCondition(rs.getString("conditions"));
			weatherlis.setComments(rs.getString("comments"));
                    
			//store all data into a List
			weatherlist.add(weatherlis);
		}

		return weatherlist;
         }

}
