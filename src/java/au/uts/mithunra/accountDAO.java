/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.uts.mithunra;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Mithun
 */
@Named
@SessionScoped
/**
 * this is the DAO for accessing the accountDTO and the loginDTO
 */
public class accountDAO implements Serializable {

    /**
     * this method is used for updating the userinfo
     * @param myInfo is passed into the method to update the user tables
     * @throws SQLException if there is a problem in connecting to the database
     * @throws NamingException is there is a problem in reachin the JNDI (jdbc/weather)
     */
    static void update(accountDTO myInfo) throws NamingException, SQLException {
        //establishes the connection to the pool
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
        Connection conn = ds.getConnection();
        String updateString = "UPDATE MITHUN.AUTHENTICATE set FIRSTNAME = ?, LASTNAME= ?, EMAIL=?, DOB=? where USERNAME = ?";
        //creates the prepared statement for the update
                PreparedStatement updateUser = conn.prepareStatement(updateString);
                updateUser.setString(1,myInfo.getFirstName());
                updateUser.setString(2,myInfo.getLastName());
                updateUser.setString(3,myInfo.getEmail());
                updateUser.setString(4,myInfo.getDob());
                updateUser.setString(5,myInfo.getUserName());
                try{
                updateUser.executeUpdate();
                conn.close();
                //pops up a success message after the update is complete
                      FacesMessage message = new FacesMessage(FacesMessage.FACES_MESSAGES, "Info updated");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
                }
                catch(SQLException e){
                                //pops up a date enter message if the update is failed
                      FacesMessage message = new FacesMessage(FacesMessage.FACES_MESSAGES, "enter a proper date");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
                }
    }
    
    /**
     * accesses the session variable session
     */
FacesContext facesContext = FacesContext.getCurrentInstance();
HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
    /**
     * this method is used for inserting the new account details from tthe signup page
     * @param DTOSignup
     * @throws SQLException if there is a problem in connecting to the database
     * @throws IOException if there is a problem in the input and output object types
     * @throws NamingException is there is a problem connecting to the JNDI (jdbc/weather)
     */
    public static void insertInto(accountDTO DTOSignup) throws SQLException, IOException, NamingException {
        //initialize the datastore
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
        Connection conn = ds.getConnection();
        //hte string for the prepared statement
        String insertString = "INSERT INTO MITHUN.AUTHENTICATE (FIRSTNAME, LASTNAME, USERNAME, PASSWORD, EMAIL, DOB)" +
	"VALUES (?, ?, ?, ?, ?, ?)";
        //creating the prepared statement to insert user account
        PreparedStatement userInsert = conn.prepareStatement(insertString);
        userInsert.setString(1,DTOSignup.getFirstName());
        userInsert.setString(2,DTOSignup.getLastName());
        userInsert.setString(3,DTOSignup.getUserName());
        userInsert.setString(4,DTOSignup.getPassword());
        userInsert.setString(5,DTOSignup.getEmail());
        userInsert.setString(6,DTOSignup.getDob());
         try{
            //tries inserting the user account
             userInsert.executeUpdate();
       FacesContext facesContext = FacesContext.getCurrentInstance();
       HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
             session.setAttribute("signin","yes");
             //closes the connection to the pool
             conn.close();
             //redirects to the login page after successful signup
ExternalContext mithun = FacesContext.getCurrentInstance().getExternalContext();
mithun.redirect(mithun.getRequestContextPath()+"/faces/login.xhtml");
        }
        catch(SQLException e){
            //in case of exception shows the error message username alreay exists
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Signup Failed", "Username Already Exists");
        RequestContext.getCurrentInstance().showMessageInDialog(message);  
        }
    }
    /**
     * this method is used for logging in the user
     * @param seshLogin used to pass the login details of the user
     * @return 
     */
    static String login(loginDTO seshLogin){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        try {
            //tries to login th euser with his details
          request.login(seshLogin.getUsername(), seshLogin.getPassword());
          System.out.println("loginnnn");
          //string fo redirection to homepage if login success
          return "viewrecords?faces-redirect=true";
          
        }
        //incase fo exception shows error message that the login failed
        catch (ServletException e) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Access Denied", "Incorrect Username or Password");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
          e.printStackTrace();
          return null;
        }
    }
/**
 * this method gets the user information from the database
 * @param seshuser used to pass the session user name to the method for getting the user info
 * @return the user information
 * @throws NamingException if there is a problem connecting with the pool JNDI (jdbc/weather)
 * @throws SQLException if there is a problem connecting to the database
 */
    static accountDTO getUserInfo(String seshuser) throws NamingException, SQLException {
        String dbName = "MITHUN";
        //creates a new useraccount varialble of type accountDTO to store the output data
        accountDTO userAccount = new accountDTO();
        //connecting to the pool
        DataSource ds = (DataSource)InitialContext.doLookup("jdbc/weather");
    Connection conn = ds.getConnection();
    //preparing the statement for queryexecute
  String userString = "select username,firstname,lastname,password,email,dob " + "from " + dbName + ".Authenticate " + "where username = ?";
    PreparedStatement getUser = conn.prepareStatement(userString);
getUser.setString(1,seshuser);
             ResultSet rs = getUser.executeQuery();
             //closing th econnection
             conn.close();
		while(rs.next()){
                    //accesses the result set and stores the data in the variable userAccount 
                        
			userAccount.setUserName(rs.getString("username"));
			userAccount.setFirstName(rs.getString("firstname"));
			userAccount.setLastName(rs.getString("lastname"));
			userAccount.setPassword(rs.getString("password"));
			userAccount.setEmail(rs.getString("email"));
			userAccount.setDob(rs.getString("dob"));
                }
		return userAccount;
    }
    
}
